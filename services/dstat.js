var nodestat = require('node-stat');
var resourcePath = '/dstat';

function sendData( client, uriComps, next )
{
	nodestat.get('stat','net','load','disk', function(err, data)
	{
		client.post(resourcePath + uriComps, data, function(err, req, res, obj){
		  return next(err, obj);
		});
	});
}

exports.sendData   = sendData;


