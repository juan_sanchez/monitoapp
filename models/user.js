var db = require("../app/db");

var userSchema = db.mongoose.Schema({
    client_id	  : { type: [String], index: true },
    client_secret : { type: [String], index: true },
	timestamp	  : { type:  String },
    token		  : { type: [String], index: true },
})

var User = db.mongoose.model('User', userSchema)


exports.User = User;
