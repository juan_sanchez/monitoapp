var db = require("../app/db");

var dstatSchema = db.mongoose.Schema({    
	hostname: {},
	stat: {},
	timestamp: { type : Date, default: Date.now }
})

var Dstat = db.mongoose.model('Dstat', dstatSchema)


exports.Dstat = Dstat;
