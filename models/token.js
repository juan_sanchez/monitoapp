var db = require("../app/db");

var userSchema = db.mongoose.Schema({
    username: { type: [String], index: true },
    password: { type: [String], index: true }
    ts: { type: Date, default: Date.now },
    token: String
})

var User = db.mongoose.model('User', userSchema)


exports.User = User;

/*
 
{
    "fullname" : "Juan Manuel",
    "username" : "Invader", 
    "email" : "usuario.jm@gmail.com",
    "token" : "123456"
}

*/
