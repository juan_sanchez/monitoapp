var POST_PATH = "/pet"; 
var PUT_PATH  = "/pet";
var GET_PATH  = "/pet/:id";
var DEL_PATH  = "/pet/:id";

function create(request, response, next){
	console.log('Creating pet...');
	
	response.send(200, 'POST OK');
	return next;
}

function read(request, response, next){
	console.log('Reading pet...');
	console.log('Id: ' + request.params.id );
	
	response.send(200, 'GET OK');
	return next;
}

function update(request, response, next){
	console.log('Updating pet...');
	
	response.send(200, 'PUT OK');
	return next;
}

function del(request, response, next){
	console.log('Deleting pet...');
	console.log('Id: ' + request.params.id );
	
	response.send(200, 'DELETE OK');
	return next;
}

exports.create = create;
exports.read   = read;
exports.update = update;
exports.del	   = del;

exports.POST_PATH = POST_PATH;
exports.PUT_PATH  = PUT_PATH;
exports.GET_PATH  = GET_PATH;
exports.DEL_PATH  = DEL_PATH;
