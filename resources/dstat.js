var dstatModel = require("../models/dstat");
var Dstat = dstatModel.Dstat;

var POST_PATH = "/dstat"; 

function create(request, response, next){
	console.log('Loging dstat data...');
	
	Dstat.create({stat: request.params.stat, hostname: request.params.hostname}, 
		function(error, dst){ 
			if(error){
				response.send(400, 'Error');
				console.log('Error: ' + error );
			}else{
				response.send(200, 'Dstat logged'); 
			}	
		}
	);
	
	return next;
}

exports.create   = create;
exports.POST_PATH  = POST_PATH;

