var userModel = require("../models/user");
var User = User = userModel.User;

var POST_PATH = "/user"; 
var PUT_PATH  = "/user/:id";
var GET_PATH  = "/user/:id";
var DEL_PATH  = "/user/:id";

function create(request, response, next){
	console.log('Creating user...');
	
	User.create(request.params, 
		function(error, usr){ 
			if(error){
				response.send(400, 'Error');
				console.log('Error: ' + error );
			}else{
				response.send(200, usr); 
			}	
		}
	);
	
	return next;
}

function read(request, response, next){
	console.log('Reading user ' + request.params.id + '...');

	usuario = User.findOne({ token: request.params.id }, 
		function(error, usr){
			if(error || usr == null){
				response.send(400, 'Error');
				console.log('Error: ' + error );
				
			}else{
				response.send(200, usr );
			}	
		}
	);
	
	return next;
}

function update(request, response, next){
	console.log('Updating user ' + request.params.id + '...');

	usuario = User.update({ token: request.params.id }, request.params,  
		function(error, usr){
			if(error || usr == null){
				response.send(400, 'Error');
				console.log('Error: ' + error );
				
			}else{
				response.send(200, usr );
			}	
		}
	);
	
	return next;
}

function del(request, response, next){
	console.log('Deleting user ' + request.params.id + '...');

	usuario = User.remove({ token: request.params.id }, 
		function(error, usr){
			if(error || usr == null){
				response.send(400, 'Error');
				console.log('Error: ' + error );
				
			}else{
				response.send(200, usr );
			}	
		}
	);
	
	return next;
}

exports.create = create;
exports.read   = read;
exports.update = update;
exports.del	   = del;

exports.POST_PATH = POST_PATH;
exports.PUT_PATH  = PUT_PATH;
exports.GET_PATH  = GET_PATH;
exports.DEL_PATH  = DEL_PATH;
