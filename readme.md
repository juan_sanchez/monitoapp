Monito App
========

Monito is a basic REST server and client implementation. It was created with remote monitoring porpuses, but can be easily adapted to take any simple CRUD model to the REST services structure. There is not big programming involved, but i tried to set an enviroment who isolates the diferent parts of the code in a simple and intuitive file structure. It takes advantage of diferent npm modules like Restify, Passport and Mongoose to do the magic.

installation
============
 
Clone this project on bitbucket:

    git clone https://juan_sanchez@bitbucket.org/juan_sanchez/monitoapp.git
	cd path/to/monito
	npm install	
	
> PREREQUISITES: MongoDB and Node.js up and and running (if you are a ubuntu/debian user you can run the install_ubuntu.sh script for that)

use
===

	node SERVER.js
	node CLIENT.js
	

