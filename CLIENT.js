var client = require("./app/client");
var baseConfig 	= require("./app/config/client/base");
var argv = require('minimist')(process.argv.slice(2));

if (argv.server)
{
	baseConfig.client.url = argv.server
	baseConfig.authClient.url = argv.server
	if (argv.port)
	{
		baseConfig.authClient.url = baseConfig.authClient.url + argv.port
	}
}	

if (argv.user)
{
	baseConfig.client_id = argv.user
}	

if (argv.pass)
{
	baseConfig.client_secret = argv.pass
}	


client.start( baseConfig );

