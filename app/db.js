var dbName = 'monitoring';
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/'+dbName);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log('Conected to database: ' + dbName );
});

exports.mongoose = mongoose;
