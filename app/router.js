function CRUDroute(resource, server, passport, strategy)
{	
	
	if(resource.POST_PATH)
	{
		server.post( resource.POST_PATH , 
			passport.authenticate(strategy, { session: false }), resource.create
		);
	}
	
	if(resource.GET_PATH)
	{
		server.get(  resource.GET_PATH , 
			passport.authenticate(strategy, { session: false }), resource.read
		);
	}
	
	if(resource.PUT_PATH)
	{
		server.put(  resource.PUT_PATH , 
			passport.authenticate(strategy, { session: false }), resource.update
		);
	}		
	
	if(resource.DEL_PATH)
	{
		server.del(  resource.DEL_PATH ,
			passport.authenticate(strategy, { session: false }), resource.del
		);	
	}
}

exports.CRUDroute = CRUDroute;
