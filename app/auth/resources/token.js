var tokenService = require('../token');
var userModel	 = require("../../../models/user");
var User = userModel.User;

var POST_PATH = "/token"; 

function create(request, response, next){
	console.log('Creating token...');

	var client 	 = {'client_id':request.user.client_id[0] , 'client_secret': request.user.client_secret[0] }

	tokenService.getToken(client, 
		function(error, token, timestamp){
			if(error)
			{
				response.send(400, 'Error');
			}	
			else
			{
				User.update(client, {'token': token, 'timestamp': timestamp},  
					function(error, usr){
						if(error || usr == null){
							response.send(400, 'Error');							
						}else{
							response.send(200, token );
						}	
					}
				);
			}	
		}
	);
		
	return next;
}

exports.create = create;
exports.POST_PATH = POST_PATH;
