var passport 	= require('passport');
var util		= require('util');
var userModel	= require('../../models/user');
var User = userModel.User;

var BearerStrategy = require('passport-http-bearer').Strategy;
var BasicStrategy = require('passport-http').BasicStrategy;

function setStrategy( type )
{
	if (type == 'basic')
	{
		return setBasicStrategy();
	}	

	if (type == 'bearer')
	{
		return SetBearerStrategy();
	}	
}

function setBasicStrategy()
{
	passport.use(new BasicStrategy(
		function(client_id, client_secret, done) 
		{
			console.log('Token request | client_id: '+ client_id +' | client_secret: ' + client_secret);
			
			User.findOne({ client_id: client_id, client_secret: client_secret }, 
			function(err, usr)
			{
				if (err) { return done(err); }
				if (!usr) { return done(null, false); }
				return done(null, usr);
			});
		})
	);
	
	return passport;
}

function SetBearerStrategy()
{	
	passport.use(new BearerStrategy({
	  },
	  function(token, done)
	  {			
			var moment = require('moment');
			var timestamp = moment().format('YYYY-MM-DD HH:00:00');
			
			User.findOne({ token: token, timestamp : timestamp }, 
			function(err, usr)
			{
				if (err) { return done(err); }
				if (!usr) { return done(Error("Invalid or outdated Token"), false); }
				return done(null, usr);
			})			
	  }
	));
	
	return passport;
}

exports.setStrategy  = setStrategy;
