var moment = require('moment');
var crypto = require("crypto"); 

function getToken(client, next){	
	var error	  = false
	var timestamp = moment().format('YYYY-MM-DD HH:00:00');
	var key 	  = client.client_id + client.client_secret + timestamp

	var hmac = crypto.createHmac("sha1", key); 
	var hash2 = hmac.update(client.client_secret); 
	
	var token = hmac.digest(encoding="base64"); 
	
	if (!token)
	{
		next(new Error("Can't create token"));
	}
	else
	{
		next(null, token ,timestamp)
	}		
}

function readToken(token)
{
	
}

exports.getToken = getToken;
