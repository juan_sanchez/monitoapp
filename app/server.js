var restify	  	=	require('restify');
var util	  	=	require('util');
var router		=	require("./router");
var strategys	=	require("./auth/strategys");

function start( config ){
	
	var server = restify.createServer(config.server);

	//Set the auth strategy 
	passport = strategys.setStrategy( config.strategy );
	
	server.use( restify.acceptParser( server.acceptable ) );
	server.use( restify.queryParser() );
	server.use( restify.bodyParser()  );
	server.use( passport.initialize() );
	
	//Routing the resources
	for(var i in config.resources ){
		resource = require( config.resourcesPath + config.resources[i] );
		router.CRUDroute(resource, server, passport, config.strategy);
	}
	
	server.listen(config.port, function () {
	  console.log('%s listening at %s', server.name, server.url);
	});
}

exports.start = start;
