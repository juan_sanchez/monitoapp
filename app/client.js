var restify = require('restify');
var _token;

function start( config )
{
	console.log("start");
	
	_sendData(config, function(unauthorized, data){
		
		if(unauthorized)
		{ 
			console.log("Outdated Token, refreshing...")
			_setToken(config, function(err, token)
			{
				if(err){ console.log("Authorization Error!"), process.exit(1) }
				_token = token;
			});
		}
	});
	
	setTimeout( function(){start(config)}, config.timeout);
}

function _sendData( config, next )
{
	var client = restify.createJsonClient( config.client );
	var uriComponents = '?hostname='+encodeURIComponent(config.hostname)+'&access_token='+encodeURIComponent(_token);
	
	for(var i in config.services ){
		service = require( config.servicesPath + config.services[i] );
		service.sendData( client, uriComponents , function(err, res)
		{
			if(err){ return next(Error("outdated")); }
			console.log(res)
		});
	}
	
	return true;
}

function _setToken( config, next )
{
	var authClient = restify.createJsonClient( config.authClient );

	authClient.basicAuth( config.client_id, config.client_secret );

	authClient.post('/token', function(err, req, res, token) {
	  if(err)
	  {
		return next(err);
	  }else{
		return next(null, token);
	  }	
	});
}

exports.start = start;
