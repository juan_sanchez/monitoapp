var server;
var port;
var strategy;
var resources;
var resourcesPath;

/* SERVER CONFIG OPTIONS
certificate		String	If you want to create an HTTPS server, pass in the PEM-encoded certificate and key
key				String	If you want to create an HTTPS server, pass in the PEM-encoded certificate and key
formatters		Object	Custom response formatters for res.send()
log				Object	You can optionally pass in a bunyan instance; not required
name			String	By default, this will be set in the Server response header, default is restify
spdy			Object	Any options accepted by node-spdy
version			String	A default version to set for all routes
handleUpgrades	Boolean	Hook the upgrade event from the node HTTP server, pushing Connection: Upgrade requests through the regular request handling chain; defaults to false
*/

server = {
	'name'		: 'Auth Server',
	'version'	: '1.0.0'
}

port = 8181;

strategy = 'basic';

resources = ['token'];

resourcesPath = './auth/resources/';

//EXPORTS
exports.server	  	  = server;
exports.port	      = port;
exports.strategy      = strategy;
exports.resources 	  = resources;
exports.resourcesPath = resourcesPath;
