var os  = require('os');

var baseClient;
var authClient;
var hostname;
var client_id;
var client_secret;
var services;
var servicesPath;
var timeout;


/* CLIENT CONFIG OPTIONS
Name			Type		Description
accept			String		Accept header to send
connect			Timeout		Number	Amount of time to wait for a socket
request			Timeout		Number	Amount of time to wait for the request to finish
dtrace			Object		node-dtrace-provider handle
gzip			Object		Will compress data when sent using content-encoding: gzip
headers			Object		HTTP headers to set in all requests
log				Object		bunyan instance
retry			Object		options to provide to node-retry;"false" disables retry; defaults to 4 retries
signRequest		Function	synchronous callback for interposing headers before request is sent
url				String		Fully-qualified URL to connect to
userAgent		String		user-agent string to use; restify inserts one, but you can override it
version			String		semver string to set the accept-version
*/

client = {
  'url'			: 'http://localhost:8080',
  'version'	: '*' 
}

authClient = {
  'url'			: 'http://localhost:8181',
  'version'	: '*'
}

client_id = 'juanchos';
client_secret = 'qwerty'; 

hostname = os.hostname();

services = ['dstat'];

servicesPath = '../services/';

timeout = 1000;

//EXPORTS
exports.client 	  	  = client;
exports.authClient 	  = authClient;
exports.client_id  	  = client_id;
exports.client_secret = client_secret;
exports.hostname      = hostname;
exports.services      = services;
exports.servicesPath  = servicesPath;
exports.timeout 	  = timeout;
